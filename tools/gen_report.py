#!/usr/bin/python3
# -*- coding: UTF-8 -*-

# 自動產生年度收入、年度支出、年度結餘三份檔案，檔名接後綴生產日期
# 例如：資安社-108學年度收入表-2020-08-26.pdf

from fpdf import FPDF
from csv import reader
import datetime
today = datetime.datetime.now()

def gen_pdf(output_name, rows):
    filename = output_name + today.strftime("-%Y-%m-%d") + '.pdf'

    pdf = FPDF()
    pdf.add_page()

    # For title
    pdf.add_font('fireflysung', '', 'fonts/fireflysung.ttf', uni = True)
    pdf.set_font('fireflysung', '', 16)
    pdf.cell(190, txt = '資安社-' + output_name, ln = 1, align = 'C')
    
    pdf.ln(10)
    # For context
    pdf.set_font('fireflysung', '', 12)
    for row in rows:
        pdf.cell(20, 10, txt = row, ln = 1, align = 'L')

    # output the file
    pdf.output(filename)

def gen_list(rows, judge_lambda):
    # return list containing string of list
    result = []
    longest_length = 0
    total = 0

    def get_longest_length(line):
        nonlocal longest_length
        if len(line) > longest_length:
            longest_length = len(line)

    for line in filter(judge_lambda, rows):
        line[4] = abs(int(line[4]))        # be careful, this will overwrite the reference
        total += line[4]
        current = str(line[::2])
        result.append(current)
        get_longest_length(current)
    
    result.append('—' * longest_length)
    result.append('總計：' + ' ' * (longest_length - len('總計：') + len(str(total))) + str(total)+ ' 元')
    return result


def gen_income(rows):
    return gen_list(rows, lambda row:int(row[4]) >= 0)

def gen_expenditure(rows):
    return gen_list(rows, lambda row:int(row[4]) < 0)

def gen_sum(rows):
    result = []
    transaction = [0, 0]
    for line in rows:
        transaction[0] += int(line[4]) if int(line[4]) > 0 else 0
        transaction[1] += int(line[4]) if int(line[4]) < 0 else 0
    
    result.append('總收入：' + str(transaction[0]) + ' 元')
    result.append('總支出：' + str(transaction[1] * -1) + ' 元')
    result.append('—' * 15)
    result.append('總計：' + str(sum(transaction)) + ' 元')

    return result

def slice_year(csv_file, year):
    list_of_csv = list(reader(csv_file))
    rows = []
    for row in list_of_csv:
        if not any(row):
            break
        try:
            if (int(row[0][0:2]) < 9 and int(row[0][6:]) == year + 1912) or (int(row[0][0:2]) >= 9 and int(row[0][6:]) == year + 1911):
                rows.append(row)
        except Exception as e:
            raise e
    return rows

if __name__ == '__main__':
    this_year = int(today.strftime("%Y")) - 1912
    select_year = input('請輸入第幾學年度(預設 ' + str(this_year) + '學年度): ')
    
    if select_year == '':
        select_year = this_year

    
    csv_file =  open('fiscalList.csv', 'r')
    rows = slice_year(csv_file, int(select_year))
    sum_rows = gen_sum(rows)
    income_rows = gen_income(rows)
    expenditure_rows = gen_expenditure(rows)
    

    gen_pdf(str(select_year) + '學年度收入表', income_rows)
    gen_pdf(str(select_year) + '學年度支出表', expenditure_rows)
    gen_pdf(str(select_year) + '學年度結餘表', sum_rows)

