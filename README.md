# temp-Tally-Book
This is the temperary Tally-book
Might be posted on block chain in the future.

{
    file name: "fiscalList.csv",
    format: {
        list: csv,
        invoice: invoice/*,
        encoding: UTF-8,
        spiltline: '\n'
        eg: "invoice create date, applicant, item name, tag name, total amount, invoice file link\n" 
    },

    transaction date: "MM-DD-YYYY",
    applicant: applicant name,
    item name: item name,
    enum ${tag}={
        "field",
        "food",
        "misc",
        "occupancy",
        "stationery",
        "teaching",
        "traffic",
        "postage",
        hardware
        },
    total amount:{
        if amount > 0:
            is income
        else:
            is fiscal charges
        },
    invoice file link: ${path}

}
